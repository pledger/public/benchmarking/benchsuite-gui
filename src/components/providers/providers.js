/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function _getKnownProperties(driver) {

  var base = ["service_properties", "grants", "name", "description", "id", "new_vm", "class", "driver"];

  if(driver=="openstack")
    return base.concat(["secret_key", "domain", "auth_url", "access_id", "region", "tenant", "auth_version"]);

  else if(driver=="vmware")
    return base.concat(["secret_password", "host", "user", "vdc", "organization"]);

  else if(driver=="ec2")
    return base.concat(["secret_key", "access_id", "region"]);

  else if(driver=="kubernetes")
    return base.concat(["api_server_url", "host_cert", "user_token", "namespace", 'auth_method', 'nodes_testing_strategy', 'nodes_to_test', 'client_cert', 'client_key']);

  return base;
}


export default _getKnownProperties;
