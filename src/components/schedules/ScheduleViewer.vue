<!--
  ~ Benchmarking Suite
  ~ Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<template>

  <v-container>

    <div>
      <v-row justify="space-between" align="center">
        <v-col>
          <v-icon large>mdi-clock-outline</v-icon>
        </v-col>
        <v-col cols="7">
          <h2>{{schedule.name}}</h2>
        </v-col>
        <v-spacer/>
        <v-col cols=4>
          <v-card-actions>
              <v-spacer/>
            <v-btn small :disabled="!enabledActions.edit" @click="$emit('editSchedule', schedule)">Edit</v-btn>
            <v-btn small :disabled="!enabledActions.clone" @click="$emit('cloneSchedule', schedule)">Clone</v-btn>
            <v-btn small :disabled="!enabledActions.delete" v-on:click="dialog=true">Delete</v-btn>
          </v-card-actions>
        </v-col>
      </v-row>
    </div>

    <v-dialog v-model="dialog" max-width="500px"
    >
      <v-card>
        <v-card-title>
          <span>Delete schedule</span>
        </v-card-title>
        <v-card-text>
        Are you sure that you want to permanently delete schedule "{{schedule.name}}"?
        <br/><br/>
        This action cannot be reverted.
        </v-card-text>
        <v-card-actions>
          <v-spacer/>
          <v-btn @click="dialog = false">Cancel</v-btn>
          <v-btn class="primary" v-on:click="deleteSchedule()">Delete</v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>


    <CommonHeader
      title="General"
      description="General information about this execution"
    />

    <v-container>
      <v-row dense>
        <v-col cols="6" >
          <div class="esy-label">Name</div> {{schedule.name}}
        </v-col>
        <v-col cols="6" >
          <div class="esy-label">ID</div> {{schedule.id}}
        </v-col>
      </v-row>
      <v-row v-if="schedule.description">
        <v-col cols="12">
          <div class="esy-label">Description</div> {{schedule.description}}
        </v-col>
      </v-row>

      <div class="esy-label">Tags</div>
      <v-chip
        :key="tag"
        v-for="(tag) in schedule.tags"
        class="ma-1"
        label
      >
      #{{tag}}
      </v-chip>

    </v-container>


    <CommonHeader
      title="Provider"
      description="The provider where the benchmark will be executed."
    />
    <v-container>

      <v-list flat>
        <ProviderSummary
          v-if="provider"
          :item="provider"
          v-on:selected="$emit('viewProvider', $event)"
        />
        <div
          v-if="provider==null"
        >
          <v-icon color="orange">mdi-alert</v-icon> You do not have sufficient permissions to access this provider
        </div>
      </v-list>

    </v-container>

    <CommonHeader
      title="Benchmarked resources"
      description="The resource types, that is VM images and their sizes, you want to benchmark."
    />

    <BenchmarkedResourcesViewer
      :entries="resources"
    />
    
    <CommonHeader
      title="Workloads"
      description="The set of workloads you want to execute on provider resources."
    />
    <v-container>
      <v-list three-line :key="wlkey">
        <WorkloadSummary
          :key="workload.data"
          v-on:selected="$emit('viewWorkload', workload)"
          v-for="(workload) in workloads"
          :item="workload"
        />
      </v-list>
    </v-container>

    <CommonHeader
      title="Configuration"
      description="Detailed configuration of workloads (e.g. parameters, properties, environment, etc.)"
    />
    <CommonHeader
      subtitle="Properties"
      description="Any parameter to set on workloads for this specific execution."
    />
    <div v-if="schedule.properties!=null && Object.keys(schedule.properties).length>0">
      <KeyValues
        :entries="this.getProperties()"
      />
    </div>
    <v-container v-else>
      No properties set
    </v-container>

    <CommonHeader
      subtitle="Environment"
      description="Environment variables to set on the VM."
    />
    <div v-if="schedule.env!=null && Object.keys(schedule.env).length>0">
      <KeyValues
        :entries="this.getEnvironment()"
      />
    </div>
    <v-container v-else>
      No variable set
    </v-container>

    <CommonHeader
      subtitle="Benchsuite additional options"
      description="Additional options for the benchmarking command line tool."
    />
    <div v-if="this.getBenchsuiteAdditionalOpts().length>0">
      <KeyValues
        :entries="this.getBenchsuiteAdditionalOpts()"
      />
    </div>
    <v-container v-else>
      No benchsuite option set
    </v-container>


    <CommonHeader
      subtitle="Docker additional options"
      description="Additional Docker options for the controller container."
    />
    <div v-if="this.getDockerAdditionalOpts().length>0">
      <KeyValues
        :entries="this.getDockerAdditionalOpts()"
      />
    </div>
    <v-container v-else>
      No Docker additional option set
    </v-container>

    
    <CommonHeader
      title="Execution"
      description="How often the benchmark is executed." 
    />
    <v-container v-if="!schedule.active">
      This benchmark is currently PAUSED
    </v-container>
    <v-container v-if="schedule.active">
      This benchmark is executed <b>every {{getFrequency()}}</b>
    </v-container>

    <template v-if="false">
      <CommonHeader
        title="Schedule sharing"
        description="Who's allowed to see this schedule"
      />
      <v-container>
        <div v-if="sharing=='public'">
          <v-icon small>mdi-earth</v-icon> {{M.SC_PUBLIC}}
        </div>
  
        <div v-if="sharing=='private'">
          <v-icon small>mdi-lock</v-icon> {{M.SC_PRIVATE}}
        </div>
  
        <div v-if="sharing=='shared'">
          <v-icon small>mdi-eye</v-icon> {{M.SC_VISIBLETOME}}
        </div>      
      </v-container>
    </template>

    <CommonHeader
      title="Results sharing"
      description="Who is allowed to see benchmarking results for this execution."
    />

    <v-container v-if="sharing=='private' || sharing=='public'">
      <div v-if="resultSharing=='public'">
        <v-icon small>mdi-earth</v-icon> {{M.SC_PUBLIC_RESULTS}}
      </div>

      <div v-if="resultSharing=='private'">
        <v-icon small>mdi-lock</v-icon> {{M.SC_PRIVATE_RESULTS}}
      </div>

      <div v-if="resultSharing=='shared'">
        <v-icon small>mdi-share-variant</v-icon> {{M.SC_SHARED_WITH_ORG}} "{{sharedWithOrg}}"
      </div>      
    </v-container>

    <v-container v-if="sharing=='shared'">
      <div v-if="resultSharing=='public'">
        <v-icon small>mdi-earth</v-icon> {{M.SC_PUBLIC_RESULTS}}
      </div>

      <div v-if="resultSharing=='private'">
        <v-icon small>mdi-eye-off</v-icon> {{M.SC_HIDDEN_RESULTS}}
      </div>

      <div v-if="resultSharing=='shared'">
        <v-icon small>mdi-eye</v-icon> {{M.SC_VISIBLETOME_RESULTS}} "{{sharedWithOrg}}"
      </div>      
    </v-container>

    <template v-if="false">
      <CommonHeader
        title="Results"
      />
      <todo>links to grafana or embedded dashboards</todo>
    </template>

  </v-container>

</template>

<script>

import CommonHeader from '../common/CommonHeader.vue';
import todo from '../common/ToDo.vue';
import ProviderSummary from '../providers/ProviderSummary.vue';
import KeyValues from '../common/KeyValues.vue';
import WorkloadSummary from '../workloads/WorkloadSummary.vue';
import BenchmarkedResourcesViewer from './BenchmarkedResourcesViewer.vue';
import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';

import { getScheduleSharingLevel, getBenchmarkResultsSharingLevel, extractSharedWithOrg } from '../common/benchsuite.js';
import { MESSAGES } from "../common/messages";

export default {

  name: 'Provider',

  components: {
    CommonHeader,
    ProviderSummary,
    KeyValues,
    WorkloadSummary,
    BenchmarkedResourcesViewer,
    todo
  },
  
  props: {
    scheduleId: {
      type: String,
      default: ''
    }
  },

  data: () => ({
    M:MESSAGES,
    schedule : {},
    dialog:false,
    provider:{},
    workloads:{},
    resources:[],
    wlkey:0,
    message: {
      show: false,
      text: "",
      title: ""
    },
    enabledActions: {
      edit: false,
      clone: false,
      delete: false
    }
  }),
  
  created () {

    this.$store.commit('fbkon');
    this.$http
      .get(this.$REST_BASE_URL + '/schedules/'+this.scheduleId)
      .then(response => {
          this.schedule = response.data;
          if(this.schedule.provider_id)
            this.loadProvider(this.schedule.provider_id);
          for(var i in this.schedule.tests) {
            if(!this.schedule.tests[i])
              continue;
            var toolName = this.schedule.tests[i].split(":")[0];
            var workloadName = this.schedule.tests[i].split(":")[1];
            this.loadWorkload(toolName, workloadName, ""+i);
          }
          this.buildResources();
          this.$emit("title", this.schedule.name);
        }
      )
      .finally(() => {
          this.$store.commit('fbkoff')
        }
      );

      // update actions
      this.$http
            .get(this.$REST_BASE_URL + '/schedules/'+this.scheduleId+"/user_actions")
        .then(response => {
          this.enabledActions.edit = response.data.edit=="True";
          this.enabledActions.clone = response.data.clone=="True";
          this.enabledActions.delete = response.data.delete=="True";
        })

  },

  mounted () {
    this.$vuetify.goTo(document.body, {duration:200})
  },
  
  computed: {
    sharing() {
      return getScheduleSharingLevel(this.schedule);
    },
    resultSharing() {
      return getBenchmarkResultsSharingLevel(this.schedule);
    },
    sharedWithOrg() {
      return extractSharedWithOrg(this.schedule);
    }
  },  
  
  methods:{
  
   getProperties: function() {
     if(this.schedule.properties)
       return Object.entries(this.schedule.properties).map(( [k, v] ) => ({ "key":k, "value":v }));
     else
       return [];
   },

   getEnvironment: function() {
     if(this.schedule.env)
       return Object.entries(this.schedule.env).map(( [k, v] ) => ({ "key":k, "value":v }));
     else
       return [];
   },

   getBenchsuiteAdditionalOpts: function() {
     if(this.schedule.benchsuite_additional_opts)
       return this.schedule.benchsuite_additional_opts.map( k => ({ "key":k, "value":"" }));
     else
       return [];
   },

   getDockerAdditionalOpts: function() {
     if(this.schedule.docker_additional_opts)
       return Object.entries(this.schedule.docker_additional_opts).map(( [k, v] ) => ({ "key":k, "value":v }));
     else
       return [];
   },

   getFrequency: function() {
     var res = ""
     for(var k in this.schedule.scheduling_hints.interval) {
       if(this.schedule.scheduling_hints.interval[k])
         res = this.schedule.scheduling_hints.interval[k] + " " + k;
     }
     
     if(this.schedule.scheduling_hints.before || this.schedule.scheduling_hints.after){
       const after  = this.schedule.scheduling_hints.after ? format(parseISO(format(new Date(), 'yyyy-MM-dd') + "T" + this.schedule.scheduling_hints.after + ".000Z"), 'HH:mm') : '00:00'
       const before  = this.schedule.scheduling_hints.before ? format(parseISO(format(new Date(), 'yyyy-MM-dd') + "T" + this.schedule.scheduling_hints.before + ".000Z"), 'HH:mm') : '24:00'
       res += " (between " + after + " and " + before + ")"
     }
     
     return res;
   },
    
   getSome: function(id) {
    return "aaa"+id+id+"aaa";
   },

   loadProvider: function(id) {
     this.$http.get(this.$REST_BASE_URL + "/providers/"+id)
    .then(response => {
        this.provider = response.data
      }
    )
    .catch(e => {
      this.provider = null;
      console.log(e)
    })
    .finally(() => {} );
   },
   
   isUUID: function(id) {
      if(!id)
        return false;
      if((id+"").length != "aefab93c-5344-4a8e-ac19-38461b029b2b".length)
        return false;
      return true;  
   },

   loadWorkload: function(t, w, ns) {
     if(this.isUUID(t)) {
       this.$http.get(this.$REST_BASE_URL + "/workloads/"+t)
      .then(response => {
          this.workloads[ns] = response.data
          this.wlkey++;
        }
      )
      .catch(e => {console.log(e)})
      .finally(() => {} );
     } else {
       this.$http.get(this.$REST_BASE_URL + "/workloads/?tool_name="+t+"&workload_name="+w)
      .then(response => {
          if(Array.isArray(response.data) && response.data.length>0) {
            this.workloads[ns] = response.data[0]
            this.wlkey++;
          }
        }
      )
      .catch(e => {console.log(e)})
      .finally(() => {} );
     }
   },
  
   deleteSchedule: function(){
    this.$store.commit('fbkon')
     this.$http.delete(this.$REST_BASE_URL + "/schedules/"+this.scheduleId)
    .then(() => {
        this.schedule = null
        this.$emit('scheduleDeleted');
      }
    )
    .catch(e => {console.log(e)})
    .finally(() => {this.$store.commit('fbkoff'); this.dialog=false} );
   },

   buildResources: function() {
    // remove existing resources
    this.resources.splice(0, this.resources.length)
    
    // a dictionary for resource types (key is image name)
    var tmp = {};

    // update with each servicetype in the schedule
    for(var i in this.schedule.service_types) {

      // create an entry for the given image name, if not already there
      var imageName = this.schedule.service_types[i].image;
      if(!tmp[imageName]) {
        tmp[imageName] = {image:{name:imageName}, flavours:[]}
        this.resources.push(tmp[imageName]);
      }
      var resource = tmp[imageName];

      // add the flavour
      if(this.schedule.service_types[i].flavour) {
        resource.flavours.push(this.schedule.service_types[i]);
      } else if(this.schedule.service_types[i].size) {
        var o = this.schedule.service_types[i];
        o.flavour = o.size;
        resource.flavours.push(o);
      }

      // add the platform
      if(this.schedule.service_types[i].platform) {
        resource.platform = this.schedule.service_types[i].platform;
      }
      
    }
    
    // sort resources
    this.resources.sort(function(a, b){
      var res = b.platform < a.platform
      if(res!=0)
        return res;
      return b.image.name < a.image.name;
    })
    
    
   }

  }

};

</script>

<style>
  div.esy-label {
    color:rgba(0, 0, 0, 0.6);
    font-size:12px;
  }
  div.esy-label-normal {
    color:rgba(0, 0, 0, 0.6);
  }

</style>
