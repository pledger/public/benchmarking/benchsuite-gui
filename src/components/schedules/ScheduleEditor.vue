<!--
  ~ Benchmarking Suite
  ~ Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<template>

  <v-container>

    <v-form
      ref="form" v-model="valid"
    >

    <MessageBox
      :model="message"
      v-on:hide="message.show=false"
    />

    <template v-if="onetime">
    </template>
    <template v-else>

      <CommonHeader
        title="General"
        description="General information about this execution"
      />

      <v-container>
        <v-row dense>
          <v-col cols="6" >
            <v-text-field
              label="Name"
              hint="The name of the schedule"
              v-model="schedule.name"
              :rules="nameRules"
              counter="50"
              required
            ></v-text-field>
          </v-col>
          <v-col cols="6" >
            <v-text-field
              label="ID"
              hint="Unique id for this schedule (not modifiable)"
              v-model="schedule.id"
              disabled
              readonly
            ></v-text-field>
          </v-col>
        </v-row>
        <v-row>
          <v-col cols="12">
            <v-text-field
              label="Description (optional)"
              hint="A description for this schedule"
              v-model="schedule.description"
              :rules="descriptionRules"
              counter="200"
              ></v-text-field>
          </v-col>
        </v-row>

        <v-combobox
            v-model="schedule.tags"
            chips
            label="Tags"
            multiple
          >
          <template v-slot:selection="{ attrs, item, select, selected }">
            <v-chip
              v-bind="attrs"
              :input-value="selected"
              close
              class="ma-1"
              label
              @click="select"
              @click:close="removeTag(item)"
            >
              #{{ item }}
            </v-chip>
          </template>
        </v-combobox>


      </v-container>

    </template>

    <CommonHeader
      title="Provider"
      description="The provider where the benchmark will be executed."
    />

    <v-container>


      <v-list three-line flat>

        <v-select
          :items="providers"
          label="Select a provider"
          item-text="name"
          item-value="name"
          return-object
          v-model="provider"
          :rules="providerRules"
          required
        ></v-select>

        <ProviderSummary
          v-if="provider.id"
          :item="provider"
        />
      </v-list>

    </v-container>

    <CommonHeader
      title="Benchmarked resources"
      description="The resource types, that is VM images and their flavours, you want to benchmark."
    />

    <BenchmarkedResourcesEditor
      :resources="resources"
      :images="images"
      :flavours="flavours"
      :loading="loadingImages"
      :disabled="provider.id==null"
    />

    <CommonHeader
      title="Workloads"
      description="The set of workloads you want to execute on provider resources."
    />

    <v-container v-if="selectedWorkloads.length>0">
      <v-list three-line>
        <WorkloadSummary
          :key="workload.data"
          v-on:delete="removeWorkload(workload)"
          v-for="(workload) in selectedWorkloads"
          :item="workload"
          remove="true"
        />
      </v-list>
    </v-container>

    <v-container>
      <v-autocomplete
        v-model="selectedWorkload"
        :items="getAvailableWorkloads()"
        chips
        color="blue-grey lighten-2"
        label="Select workloads"
        item-text="_text"
        return-object
        hide-selected
        :allow-overflow=false
        v-on:change="addSelectedWorkload()"
        :error-messages="errorMessages.workloads"
        v-on:blur="validateWorkloads()"
      >

      <template v-slot:item="workloads">
          <v-list-item-avatar>
            <v-icon>mdi-tape-measure</v-icon>
          </v-list-item-avatar>

          <v-list-item-content>
            <v-list-item-title>
              {{workloads.item.name}} ({{workloads.item.tool}})
            </v-list-item-title>

            <v-list-item-subtitle class="wrap-text" style="width:100px">
              {{workloads.item.description}}
            </v-list-item-subtitle>

            <div align="right">
              <v-chip
                v-for="(cat) in workloads.item.categories"
                :key="cat"
                class="ma-1"
                small
              >#{{cat}}</v-chip>
              <v-chip small class="ma-1"><v-icon small left>mdi-earth</v-icon>public</v-chip>
            </div>
          </v-list-item-content>

      </template>

      </v-autocomplete>

    </v-container>


    <CommonHeader
      title="Configuration"
      description="Detailed configuration of workloads (e.g. parameters, properties, environment, etc.)"
    />
    <CommonHeader
      subtitle="Properties"
      description="Any parameter to set on workloads for this specific execution."
    />

    <KeyValues
      :entries="properties"
      keyHint="The name of the property"
      valueHint="The value of the property"
      invalidChars=" "
      addButtonLabel="Add property"
      :readonly="false"
    />

    <CommonHeader
      subtitle="Environment"
      description="Environment variables to set on the VM."
    />

    <KeyValues
      :entries="environment"
      keyHint="The name of the variable"
      valueHint="The value of the variable"
      invalidChars=" "
      addButtonLabel="Add variable"
      :readonly="false"
    />

    <CommonHeader
      subtitle="Benchsuite additional options"
      description="Additional options for the benchmarking command line tool."
    />

    <KeyValues
      :entries="additionalOptions"
      keyLabel="Name"
      keyHint="The name of the option"
      valueLabel="Value (optional)"
      :optionalValue="true"
      valueHint="The value of the option (if any)"
      invalidChars=" "
      addButtonLabel="Add option"
      :readonly="false"
    />

    <CommonHeader
      subtitle="Docker additional options"
      description="Additional Docker options for the controller container."
    />

    <KeyValues
      :entries="dockerAdditionalOptions"
      keyLabel="Name"
      :optionalValue="true"
      keyHint="The name of the option"
      valueLabel="Value (optional)"
      valueHint="The value of the option (if any)"
      invalidChars=" "
      addButtonLabel="Add option"
      :readonly="false"
    />

    <template v-if="onetime==false">

      <CommonHeader
        title="Execution"
        description="How often the benchmark is executed."
      />

      <v-container>
        <v-row dense align="center" >
          <v-col cols="1">
            <v-switch
              align="right"
              v-model="schedule.active"
            />
          </v-col>
          <v-col cols="3">
            <div
              v-bind:class="{ 'text--disabled': !schedule.active}"
            >
              Execute this benchmark every
            </div>
          </v-col>
          <v-col cols="3">
            <v-text-field
              style="text-align:right"
              label="Interval"
              :disabled="!schedule.active"
              v-model="interval.every"
              :rules="schedule.active ? scheduleIntervalRules : []"
            ></v-text-field>
          </v-col>
          <v-col>
            <v-select
              label="Unit"
              :disabled="!schedule.active"
              :items="scheduleFrequencies"
              v-model="interval.unit"
              :rules="schedule.active ? scheduleUnitRules : []"
            ></v-select>
          </v-col>
        </v-row>
        <v-row>
          <v-col><div v-bind:class="{ 'text--disabled': !schedule.active}">Starts after</div></v-col>
          <v-col
          >
            <v-menu
              ref="menu"
              v-model="menu"
              :disabled="!schedule.active"
              :close-on-content-click="false"
              :nudge-right="40"
              :return-value.sync="scheduleAfterTime"
              transition="scale-transition"
              offset-y
              max-width="290px"
              min-width="290px"
            >
              <template v-slot:activator="{ on, attrs }">
                <v-text-field
                  v-model="scheduleAfterTime"
                  label="Picker in menu"
                  :disabled="!schedule.active"
                  prepend-icon="mdi-clock-time-four-outline"
                  readonly
                  v-bind="attrs"
                  v-on="on"
                ></v-text-field>
              </template>
              <v-time-picker
                format="24hr"
                v-if="menu"
                v-model="scheduleAfterTime"
                full-width
                @click:minute="$refs.menu.save(scheduleAfterTime)"
              ></v-time-picker>
              <v-btn @click="$refs.menu.save(null)">Unset</v-btn>
            </v-menu>
          </v-col>
          <v-col><div v-bind:class="{ 'text--disabled': !schedule.active}">and before</div></v-col>
          <v-col
          >
            <v-menu
              ref="menu2"
              v-model="menu2"
              :disabled="!schedule.active"
              :close-on-content-click="false"
              :nudge-right="40"
              :return-value.sync="scheduleBeforeTime"
              transition="scale-transition"
              offset-y
              max-width="290px"
              min-width="290px"
            >
              <template v-slot:activator="{ on, attrs }">
                <v-text-field
                  v-model="scheduleBeforeTime"
                  label="Picker in menu"
                  :disabled="!schedule.active"
                  prepend-icon="mdi-clock-time-four-outline"
                  readonly
                  v-bind="attrs"
                  v-on="on"
                ></v-text-field>
              </template>
              <v-time-picker
                format="24hr"
                v-if="menu2"
                v-model="scheduleBeforeTime"
                full-width
                @click:minute="$refs.menu2.save(scheduleBeforeTime)"
              ></v-time-picker>
              <v-btn @click="$refs.menu2.save(null)">Unset</v-btn>
            </v-menu>
          </v-col>
        </v-row>
      </v-container>

    </template>

    <template v-if="false">
      <CommonHeader
        title="Schedule Sharing"
        description="Who can see this workload"
      />
      <v-container>
        <v-radio-group v-model="sharing">
          <v-radio value="private"
            :label="M.SC_PRIVATE"
          >
          </v-radio>
          <v-radio value="public"
            :label="M.SC_PUBLIC"
          >
          </v-radio>
        </v-radio-group>
      </v-container>
    </template>

    <CommonHeader
      title="Results sharing"
      description="Who is allowed to see benchmarking results for this execution."
    />
    <v-container>
      <v-radio-group v-model="resultSharing">
        <v-radio value="private"
          :label="M.SC_PRIVATE_RESULTS"
        >
        </v-radio>
        <v-radio value="shared"
          v-if="sharing=='shared'"
          :label="M.SC_VISIBLETOME_RESULTS"
        >{{sharedWithOrg}}
        </v-radio>
        <v-radio value="shared" v-if="sharing=='private' || sharing=='public'">
          <template v-slot:label>
            <span style="padding-right:8px">{{M.SC_SHARED_WITH_ORG}}</span>
            <v-text-field
              dense
              style="position:relative; top:3px"
              :disabled="resultSharing!='shared'"
              hint="The name of the organization"
              v-model="sharedWithOrg"
              :rules="resultSharing=='shared' ? resultSharingRules : []"
            ></v-text-field>
          </template>
        </v-radio>
        <v-radio value="public"
          :label="M.SC_PUBLIC_RESULTS"
        >
        </v-radio>
      </v-radio-group>
    </v-container>

    <template v-if="false">

      <CommonHeader
        title="Results"
      />

      <todo>Link to results for this scheduling, or embedded dashboards</todo>

    </template>

      <v-divider/>

      <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn @click="exit()">Back</v-btn>
          <v-btn
            v-if="onetime"
            color="primary"
            :disabled="!valid"
            v-on:click="save(true)"
          >Execute</v-btn>
          <v-btn
            v-else-if="schedule.id==null"
            color="primary"
            :disabled="!valid"
            v-on:click="save(true)"
          >Create & Exit</v-btn>
          <v-btn
            v-else
            color="primary"
            :disabled="!valid"
            v-on:click="save(true)"
          >Save & Exit</v-btn>
      </v-card-actions>

    </v-form>

  </v-container>

</template>

<script>

import CommonHeader from '../common/CommonHeader.vue';
import ProviderSummary from '../providers/ProviderSummary.vue';
import todo from '../common/ToDo.vue';
import KeyValues from '../common/KeyValues.vue';
import BenchmarkedResourcesEditor from './BenchmarkedResourcesEditor.vue';
import WorkloadSummary from '../workloads/WorkloadSummary.vue';
import MessageBox from '../common/MessageBox.vue';
import parse from 'date-fns/parse';
import format from 'date-fns/format';
import parseISO from 'date-fns/parseISO';
import { formatInTimeZone } from 'date-fns-tz';

import { MESSAGES } from "../common/messages";

import { getProviderSharingLevel, getScheduleSharingLevel, getBenchmarkResultsSharingLevel, extractSharedWithOrg, trimStringValues } from '../common/benchsuite.js';

export default {

  name: 'ScheduleEditor',

  props: {
    scheduleId: {
      type: String,
      default: ''
    },
    executionId: {
      type: String,
      default: ''
    },
    templateScheduleId: {
      type: String,
      default: ''
    },
    scheduleTemplate: {
      type: Object,
      default: function() {return {}}
    },
    onetime: {
      type: Boolean,
      default: false
    }
  },

  components: {
    CommonHeader,
    ProviderSummary,
    KeyValues,
    BenchmarkedResourcesEditor,
    WorkloadSummary,
    todo,
    MessageBox
  },

  data: () => ({

    M:MESSAGES,

    // the input/output object
    schedule : {},

    // the selected provider
    provider: {},

    // benchmarked resources
    resources: [],

    properties: [],
    environment: [],
    additionalOptions: [],
    dockerAdditionalOptions: [],

    providers: [],

    sharing : "private",
    resultSharing : "private",
    sharedWithOrg : "",

    images: [],
    flavours: [],

    interval: {
      every: 1,
      unit: "days"
    },

    scheduleBeforeTime: null,
    scheduleAfterTime: null,
    menu: false,
    menu2: false,

    workloads: [],

    selectedWorkloads: [],

    selectedWorkload: {},

    loadingImages: false,
    loadingflavours: false,

    providerData : {},
    customProperties : [],

    scripts: [],

    valid : false,

    errorMessages: {
      "workloads": []
    },

    reservedKeys : ["create_time", "update_time", "secret_key", "name", "description", "id", "domain", "new_vm", "auth_url", "post_create_script", "access_id", "class", "region", "tenant", "driver", "auth_version"],

    scheduleFrequencies : [
      { text:"Minute(s)", value:"minutes"},
      { text:"Hour(s)", value:"hours"},
      { text:"Day(s)", value:"days"},
      { text:"Week(s)", value:"weeks"}
    ],

    message: {
      show: false,
      text: "",
      title: ""
    },

    nameRules: [
        v => !!v || 'A name is required',
        v => (v && v.length <= 50) || 'Name must be less than 50 characters',
        v => (v && v.trim().length > 0) || 'A name is required'
      ],

    descriptionRules: [
        v => !v || (v && v.length <= 200) || 'Description must be less than 200 characters',
      ],

    technologyRules: [
        v => !!v || 'A cloud provider technology must be selected'
      ],

    authenticationRules : [
        v => !!v || 'An authentication method must be selected'
      ],

    hostRules : [
        v => !!v || 'A connection host is required'
      ],

    usernameRules : [
        v => !!v || 'A user name is required'
      ],

    passwordRules : [
        v => !!v || 'A password/secret is required'
      ],

    providerRules: [
        v => !!v.id || 'A provider is required',
      ],

    resultSharingRules: [
        v => !!v || 'An organization name is required',
      ],

    scheduleIntervalRules : [
        v => !!v || 'An interval is required',
        v => Number.isInteger(Number(v)) || "A number is required",
        v => Number(v) > 0 || "A positive number is required",
      ],

    scheduleUnitRules : [
        v => !!v || 'A unit is required'
      ]

  }),

  watch: {
    providerData: {
      deep: true,
      handler: function(val) {
        if(val.driver!="openstack") {
          delete this.providerData.auth_version;
        }
      }
    },
    schedule: {
      deep:true,
      handler: function() {
      }
    },

    provider: {
      handler: function() {
        this.loadImages(this.provider.id);
        this.loadflavours(this.provider.id);
        // FIXME: the following creates a problem when going in edit mode (resources are removed after the provider is set)
//        this.resources.splice(0, this.resources.length)
      }
    },

    selectedWorkloads: {
      deep:true,
      handler: function() {
        this.validateWorkloads();
      }
    }

  },

  mounted () {
    if(this.scheduleTemplate && this.scheduleTemplate.name) {
      this.schedule = this.scheduleTemplate;
      // make sure some properties are set
      if(!this.schedule.tests)
        this.schedule.tests = [];
      if(this.schedule.active==null)
        this.schedule.active = false;
      if(this.schedule.scheduling_hints.interval==null)
        this.schedule.scheduling_hints.interval = {};
      this.buildGuiHelpers();
      this.sharing = getScheduleSharingLevel(this.schedule);
      this.resultSharing = getBenchmarkResultsSharingLevel(this.schedule);
      this.sharedWithOrg = extractSharedWithOrg(this.schedule);
    } else if(this.executionId) {
      this.$http
        .get(this.$REST_BASE_URL + '/executions/'+this.executionId+'/request')
        .then(
          response => {
            this.schedule = response.data;
            // make sure some properties are set
            if(!this.schedule.tests)
              this.schedule.tests = [];
            if(this.schedule.active==null)
              this.schedule.active = false;
            if(this.schedule.scheduling_hints.interval==null)
              this.schedule.scheduling_hints.interval = {};
            this.buildGuiHelpers();
            this.sharing = getScheduleSharingLevel(this.schedule);
            this.resultSharing = getBenchmarkResultsSharingLevel(this.schedule);
            this.sharedWithOrg = extractSharedWithOrg(this.schedule);
          }
        )
        .finally(() =>
           {this.$vuetify.goTo(document.body, {duration:200})}
        );
    } else if(this.scheduleId) {
      this.$http
        .get(this.$REST_BASE_URL + '/schedules/'+this.scheduleId)
        .then(
          response => {
            this.schedule = response.data;
            this.buildGuiHelpers();
            this.sharing = getScheduleSharingLevel(this.schedule);
            this.resultSharing = getBenchmarkResultsSharingLevel(this.schedule);
            this.sharedWithOrg = extractSharedWithOrg(this.schedule);
          }
        )
        .finally(() =>
           {this.$vuetify.goTo(document.body, {duration:200})}
        );
    } else if(this.templateScheduleId) {
      this.$http
        .get(this.$REST_BASE_URL + '/schedules/'+this.templateScheduleId)
        .then(
          response => {
            this.schedule = response.data;
            this.schedule.id=null;
            this.schedule.name='Copy of '+this.schedule.name;
            this.schedule.grants={owner:'me'}
            this.buildGuiHelpers();
            this.sharing = getScheduleSharingLevel(this.schedule);
            this.resultSharing = getBenchmarkResultsSharingLevel(this.schedule);
            this.sharedWithOrg = extractSharedWithOrg(this.schedule);
          }
        )
        .finally(() =>
           {this.$vuetify.goTo(document.body, {duration:200})}
        );
    } else {
      this.schedule = Object.assign({}, this.schedule, { name: "New Schedule", tests:[], active:false, scheduling_hints: {interval:{}}, grants:{owner:"me"} })
      this.buildGuiHelpers();
      this.sharing = getScheduleSharingLevel(this.schedule);
      this.resultSharing = getBenchmarkResultsSharingLevel(this.schedule);
      this.sharedWithOrg = extractSharedWithOrg(this.schedule);
    }
   this.$vuetify.goTo(document.body, {duration:200})
  },

  methods:{

    extractSharedWithOrg: function() {
      if(!this.schedule["reports-scope"])
        return "";
      var sharedWith = this.schedule["reports-scope"];
      if(sharedWith.startsWith("usr:"))
        return "";
      if(sharedWith=="org:public")
        return "";
      if(sharedWith.startsWith("org:"))
        return sharedWith.substring(4);
      return "";
    },

    buildGuiHelpers: function() {
      if(this.schedule.provider_id)
        this.loadProvider(this.schedule.provider_id);
      this.loadProviders();
      this.buildProperties();
      this.buildEnvironment();
      this.buildAdditionalOpts();
      this.buildResources();
      this.buildDockerAdditionalOpts();
      this.buildInterval();
      this.loadWorkloads();
   },

   removeWorkload: function(workload) {
     for(var i in this.selectedWorkloads) {
//       if(this.selectedWorkloads[i].workload_name==workload.workload_name
//            && this.selectedWorkloads[i].tool_name==workload.tool_name
//         ) {
       if(this.selectedWorkloads[i].id == workload.id) {
         this.selectedWorkloads.splice(i, 1);
         break;
       }
     }
   },

   workloadSelected: function(workload) {
     for(var i in this.selectedWorkloads) {
//       if(this.selectedWorkloads[i].workload_name==workload.workload_name
//            && this.selectedWorkloads[i].tool_name==workload.tool_name
//         ) {
       if(this.selectedWorkloads[i].id == workload.id) {
         return true;
       }
     }
     return false;
   },

   getAvailableWorkloads: function() {
     var out = [];
     for(var i in this.workloads) {
       if(!this.workloadSelected(this.workloads[i])) {
         out.push(this.workloads[i]);
       }
     }
     return out;
   },

   validateWorkloads: function() {
      this.errorMessages.workloads.splice(0, this.errorMessages.workloads.length)
      if(this.selectedWorkloads.length==0) {
        this.errorMessages.workloads.push("At least workload type is required");
      }
   },

   loadImages: function(providerId) {
     this.loadingImages = true;
     this.$http.get(this.$REST_BASE_URL + "/providers/"+providerId+"/vm/images")
    .then(response => {
        this.images = response.data;
        this.images.sort(function(a, b){
          return (a.label.toLowerCase() < b.label.toLowerCase() ? -1 : 1);
        })
      }
    )
    .catch(e => {
      console.log(e)
      this.images = [];
    })
    .finally(() => {
      this.loadingImages = false;
    } );
   },

   loadflavours: function(providerId) {
     this.$http.get(this.$REST_BASE_URL + "/providers/"+providerId+"/vm/flavours")
    .then(response => {
        this.flavours = [];
        for(var i in response.data) {
          var inFlavour = response.data[i];
          var outFlavour = {};
          outFlavour.flavour = inFlavour.name;
          for(var j in inFlavour.params) {
            var param = inFlavour.params[j];
            outFlavour[param.name] = param.value;
          }
          this.flavours.push(outFlavour);
        }
      }
    )
    .catch(e => {
      console.log(e)
      this.flavours = [];
    })
    .finally(() => {} );
   },

   loadProviders: function() {
     this.$http.get(this.$REST_BASE_URL + "/providers")
    .then(response => {
        this.providers = response.data;

        // filter out providers shared with me
        this.providers = this.providers.filter(item => {
          var sharingPolicy = getProviderSharingLevel(item);
          return sharingPolicy=="public" || sharingPolicy=="private";
        })

        // sort them alphabetically
        this.providers.sort(function(a, b){
          if(a.name < b.name) {
            return -1;
          } else if(a.name > b.name) {
            return 1;
          } else {
            return a.id > b.id ? 1 : -1;
          }
        })
      }
    )
    .catch(e => {console.log(e)})
    .finally(() => {} );
   },

   addSelectedWorkload: function() {
     if(!this.workloadSelected(this.selectedWorkload)) {
       this.selectedWorkloads.push(this.selectedWorkload);
       this.selectedWorkload = {};
     }
   },

   loadWorkloads: function() {
     this.$http.get(this.$REST_BASE_URL + "/workloads/?embed_parent=true&resolve=true")
    .then(response => {

        this.workloads = [];
        // add only non-abstract workloads
        for(var i in response.data) {
          var wl = response.data[i];
          if(!wl.abstract)
            this.workloads.push(wl);
        }

        this.buildWorkloads();

        // build a property to support autocomplete
        for(i in this.workloads) {
          wl = this.workloads[i];
          wl._text = wl.name + " " + wl.tool + " " + wl.description;
          for(var c in wl.categories) {
            wl._text += " " + wl.categories[c];
          }
        }

        this.workloads.sort(function(a, b){
          if(a.name < b.name) {
            return -1;
          } else if(a.name > b.name) {
            return 1;
          } else if(a.tool < b.tool) {
            return -1;
          } else if(a.tool > b.tool) {
            return 1;
          } else {
            return a.id > b.id ? 1 : -1;
          }
        })
      }
    )
    .catch(e => {console.log(e)})
    .finally(() => {} );
   },

   removeTag: function(tag) {
     var index = this.schedule.tags.indexOf(tag);
     if (index > -1) {
       this.schedule.tags.splice(index, 1);
     }
   },

   buildInterval: function() {
     // to manage active=null
     this.schedule.active = (this.schedule.active==true);

     if(this.schedule.scheduling_hints.interval) {
       for(var k in this.schedule.scheduling_hints.interval) {
         var v = this.schedule.scheduling_hints.interval[k];
         if(k!=null & v!=null) {
           this.interval.every = v;
           this.interval.unit = k;
         }
       }
     }

     if(this.schedule.scheduling_hints.before) {
       this.scheduleBeforeTime = format(parseISO(format(new Date(), 'yyyy-MM-dd') + "T" + this.schedule.scheduling_hints.before + ".000Z"), 'HH:mm')
     }
     if(this.schedule.scheduling_hints.after) {
      this.scheduleAfterTime = format(parseISO(format(new Date(), 'yyyy-MM-dd') + "T" + this.schedule.scheduling_hints.after + ".000Z"), 'HH:mm')
     }
   },

   buildProperties: function() {
    this.properties.splice(0, this.properties.length)
    for(var k in this.schedule.properties) {
      if(this.schedule.properties[k]) {
        this.properties.push({"key":k, "value":this.schedule.properties[k]});
      }
    }
   },

   buildWorkloads: function() {
    this.selectedWorkloads.splice(0, this.selectedWorkloads.length)
    for(var k in this.schedule.tests) {
      var test = this.schedule.tests[k];
      if(test) {
        // search workload among the cached ones
        for(var i in this.workloads) {
          if(this.workloads[i].id == test) {
            this.selectedWorkloads.push(this.workloads[i]);
          }
        }
//        this.selectedWorkloads.push({tool_name:test.split(":")[0], workload_name:test.split(":")[1]});
      }
    }
   },

   buildAdditionalOpts: function() {
    this.additionalOptions.splice(0, this.additionalOptions.length)
    for(var k in this.schedule.benchsuite_additional_opts) {
      this.additionalOptions.push({"key":this.schedule.benchsuite_additional_opts[k], "value":""});
    }
   },

   buildDockerAdditionalOpts: function() {
    this.dockerAdditionalOptions.splice(0, this.dockerAdditionalOptions.length)
    for(var k in this.schedule.docker_additional_opts) {
      this.dockerAdditionalOptions.push({"key":k, "value":this.schedule.docker_additional_opts[k]});
    }
   },

   buildEnvironment: function() {
    this.environment.splice(0, this.environment.length)
    for(var k in this.schedule.env) {
      if(this.schedule.env[k]) {
        this.environment.push({"key":k, "value":this.schedule.env[k]});
      }
    }
   },

   buildResources: function() {
    // remove existing resources
    this.resources.splice(0, this.resources.length)

    // a dictionary for resource types (key is image name)
    var tmp = {};

    // update with each servicetype in the schedule
    for(var i in this.schedule.service_types) {

      // create an entry for the given image name, if not already there
      var imageName = this.schedule.service_types[i].image;
      var imageId = this.schedule.service_types[i].imageId;
      if(!tmp[imageName]) {
        tmp[imageName] = {image:{label:imageName, value:imageId}, flavours:[]}
        this.resources.push(tmp[imageName]);
      }
      var resource = tmp[imageName];

      // add the flavour
      if(this.schedule.service_types[i].flavour) {
        resource.flavours.push(this.schedule.service_types[i]);
      }

      // add the platform
      if(this.schedule.service_types[i].platform) {
        resource.platform = this.schedule.service_types[i].platform;
      }

    }
   },

   loadProvider: function(id) {
     this.$http.get(this.$REST_BASE_URL + "/providers/"+id)
    .then(response => {
        this.provider = response.data
      }
    )
    .catch(e => {console.log(e)})
    .finally(() => {} );
   },

   getReservedKeys: function() {
      return this.reservedKeys;
   },

   isKnownProperty: function(key) {
    return this.getReservedKeys().includes(key)
   },

   isCustomProperty: function(key) {
    return !this.isKnownProperty(key);
   },

   create: function(test, exit){
    this.$store.commit('fbkon')
    this.$http.post(this.$REST_BASE_URL + "/providers/", this.providerData)
    .then(response => {
        this.providerData = Object.assign({}, this.providerData, response.data)
        if(test)
          this.test();
        if(exit)
          this.exit(this.providerData.id);
      }
    )
    .catch(e => {console.log(e)})
    .finally(() => {this.$store.commit('fbkoff')});
   },

   packForSave: function() {
     // name is already ok

     // description is already ok

     // tests
     this.schedule.tests.splice(0, this.schedule.tests.length);
     for(var i in this.selectedWorkloads) {
       var workload = this.selectedWorkloads[i]
//       this.schedule.tests.push(workload.tool_name+":"+workload.workload_name);
       this.schedule.tests.push(workload.id);
     }

     // tags
     if(!this.schedule.tags)
       this.schedule.tags = [];

     // properties
     this.schedule.properties = {};
     for(i in this.properties) {
       var p = this.properties[i];
       this.schedule.properties[p.key] = p.value;
     }

     // env
     this.schedule.env = {};
     for(i in this.environment) {
       var e = this.environment[i];
       this.schedule.env[e.key] = e.value;
     }

     // bench_add_opts
     this.schedule.benchsuite_additional_opts = [];
     for(i in this.additionalOptions) {
       var bopt = this.additionalOptions[i];
       this.schedule.benchsuite_additional_opts.push(bopt.key);
     }

     // dock add opts
     this.schedule.docker_additional_opts = {};
     for(i in this.dockerAdditionalOptions) {
       var dopt = this.dockerAdditionalOptions[i];
       this.schedule.docker_additional_opts[dopt.key] = dopt.value;
     }

     // active is already ok

     // interval
     this.schedule.scheduling_hints = {interval: {}};
     this.schedule.scheduling_hints.interval[this.interval.unit] = this.interval.every;

     if(this.scheduleBeforeTime) {
       this.schedule.scheduling_hints['before'] = formatInTimeZone(parse(this.scheduleBeforeTime,"HH:mm", new Date()), 'UTC', 'HH:mm:ss')
     }
     if(this.scheduleAfterTime) {
       this.schedule.scheduling_hints['after'] = formatInTimeZone(parse(this.scheduleAfterTime,"HH:mm", new Date()), 'UTC', 'HH:mm:ss')
     }

     // provider id
     this.schedule.provider_id = this.provider.id;

     // service types
     this.schedule.service_types = [];
     for(i in this.resources) {
       var r = this.resources[i];
       for(var j in r.flavours) {
         var serviceType = {};

         // add all properties from the flavour
         serviceType = {...serviceType, ...r.flavours[j]}

         // rename the flavour name as 'size' and 'flavour'
         delete serviceType.name;
         serviceType.size = r.flavours[j].flavour;
         serviceType.flavour = r.flavours[j].flavour;

         // finally, add the image
         serviceType.platform = r.platform;

         // finally, add the image
         serviceType.image = r.image.label;
         serviceType.imageId = r.image.value;

         this.schedule.service_types.push(serviceType);
       }
     }

     // set grants
     if(this.sharing=="private") {
       this.schedule.grants = {};
     } else if(this.sharing=="public") {
       this.schedule.grants = {viewer:"org:public"};
     } else {
       this.schedule.grants = {};
     }

     // set results sharing
     if(this.resultSharing=="private") {
       this.schedule["reports-scope"] = "usr:me";
     } else if(this.resultSharing=="public") {
       this.schedule["reports-scope"] = "org:public";
     } else if(this.resultSharing=="shared") {
       this.schedule["reports-scope"] = "org:" + this.sharedWithOrg;
     } else {
       console.log("don't know what to do " + this.resultSharing)
     }

     delete this.schedule.provider;
     delete this.schedule.workloads;

     trimStringValues(this.schedule);

   },

   save: function(exit){

    this.packForSave();

    // set feedback ON
    this.$store.commit('fbkon')

    // create or update (depending on id)
    if(this.schedule.id==null) {
      this.$http.post(this.$REST_BASE_URL + "/schedules", this.schedule)
      .then((response) => {
          this.schedule.id = response.data.id;
          if(exit)
            this.exit(this.schedule.id);
        }
      )
      .catch(e => {console.log(e)})
      .finally(() => {this.$store.commit('fbkoff')});
    } else {
      this.$http.put(this.$REST_BASE_URL + "/schedules/"+this.schedule.id, this.schedule)
        .then(() => {
          if(exit)
            this.exit(this.schedule.id);
        }
      )
      .catch(e => {console.log(e)})
      .finally(() => {this.$store.commit('fbkoff')});
    }

   },

   exit: function(id) {
      id = id || this.scheduleId || this.templateScheduleId;
      if(this.onetime)
        this.$emit("editingExecutionComplete", {id:id})
      else
        this.$emit("editingScheduleComplete", {id:id})
   }

 }

};

</script>
