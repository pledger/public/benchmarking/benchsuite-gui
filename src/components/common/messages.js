/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const MESSAGES = {

  CREATE_AND_EXIT : "Create & Exit",
  SAVE_AND_EXIT : "Save & Exit",
  BACK : "Back",
  FILTER: "Filter",
  
  // labels
  WL_WORKLOAD_NAME : "Workload name",
  WL_TOOL_NAME : "Tool name",
  WL_ID : "Workload ID",
  WL_WORKLOAD_DESCRIPTION : "Workload description (optional)",
  WL_CATEGORIES : "Categories",
  WL_NO_INSTALL_SCRIPTS : "No 'install' scripts given",
  WL_NO_POSTINSTALL_SCRIPTS : "No 'postinstall' scripts given",
  WL_NO_EXEC_SCRIPTS : "No 'exec' scripts given",
  WL_NO_CLEANUP_SCRIPTS : "No 'cleanup' scripts given",
  WL_NO_PARAMETERS : "No parameters",

  WL_PRIVATE: "Private: only you can view and use this workload.",
  WL_PUBLIC: "Publicly visible: you own this workload. Anybody can execute it on their own providers.",
  WL_VISIBLETOME: "Shared with you: this workload is owned by someone else. You are only allowed to view and execute it.",

  PR_PRIVATE: "Private: only you can view and use this provider. It won't appear on any result.",
  PR_PUBLIC: "Publicly visible: you own this provider. Only you can run benchmarks on this provider. However, it is publicly visible and it's name and some metadata can appear along with public results.",
  PR_VISIBLETOME: "Shared with you: this provider is owned by someone else. You are only allowed to view some metadata.",

  SC_PRIVATE: "Private: only you can view and use this schedule.",
  SC_PUBLIC: "Publicly visible: you own this schedule. Only you can run this schedule, but everybody can see it.",
  SC_VISIBLETOME: "Shared with you: this schedule is owned by someone else. You are only allowed to view it.",

  SC_PUBLIC_RESULTS: "Public: metrics are visible to everybody.",
  SC_HIDDEN_RESULTS: "Hidden: metrics are not visible to you.",
  SC_PRIVATE_RESULTS: "Private: metrics are visible to you only.",
  SC_VISIBLETOME_RESULTS: "Shared: metrics are visible to you as a member of organization ",
  SC_SHARED_WITH_ORG: "Shared: metrics are visible to all members of organization ",

  WL_SCRIPT_NAME : "Script name",
  WL_SCRIPT_VALUE : "Commands",

  // buttons
  WL_ADD_SCRIPT : "Add script",
  WL_ADD_PARAMETER : "Add parameter",  
    
  // default values for new workload
  WL_DEFAULT_WORKLOADNAME : "New workload",
  WL_DEFAULT_TOOLNAME : "New tool",
  
  // sections
  WL_GENERAL_SECTION_TITLE : "General",
  WL_GENERAL_SECTION_DESCRIPTION : "General workload metadata, to help you and others to find and select them.",
  WL_EXECUTION_SECTION_TITLE : "Workload execution",
  WL_EXECUTION_SECTION_DESCRIPTION : "Commands ran to setup the benchmark environment, execute the benchmark and cleanup the environment afterwards. Platform-specific scripts are defined by prefixing the script with the platform name and, optionally, a version (e.g. 'install_ubuntu_20').",
  WL_INSTALL_SECTION_TITLE : "1. Install",
  WL_POSTINSTALL_SECTION_TITLE : "2. Post-Install",
  WL_EXECUTE_SECTION_TITLE : "3. Execute",
  WL_CLEANUP_SECTION_TITLE : "4. Cleanup",
  WL_USER_SCRIPTS_SECTION_TITLE : "User and support scripts",
  WL_PARAMETERS_SECTION_TITLE : "Workload parameters",
  WL_SHARING_SECTION_TITLE : "Sharing", 
  WL_SHARING_SECTION_DESCRIPTION : "Visibility level for this workload.",
  WL_RESULTS_SECTION_TITLE : "Results",
  WL_ALL_WORKLOADS : "All Workloads",
  WL_ALL_WORKLOADS_DESCRIPTION : "This section contains all workloads, public and private, you can run on your providers.",
  
  PR_GENERAL_SECTION_TITLE : "General",
  PR_GENERAL_SECTION_DESCRIPTION: "General information is used to quickly identify the provider. Name and description are the only information that might be disclosed to other users, in case you share benchmarking metrics with others.",
  PR_CONNECTION_SECTION_TITLE : "Connection",
  PR_CONNECTION_SECTION_DESCRIPTION: "Provider coordinates, needed to create virtual machines.",
  PR_CUSTOM_PROPS_SECTION_TITLE : "Custom properties",
  PR_CUSTOM_PROPS_SECTION_DESCRIPTION: "Depending on the provider technology, additional connection properties might be needed.",
  PR_POST_CREATE_SCRIPTS_SECTION_TITLE : "Post-create scripts",
  PR_POST_CREATE_SCRIPTS_SECTION_DESCRIPTION: "Post-create scripts are executed after the provisioning of the virtual machine to perform some general initialization (e.g. configure the DNS).",
  
  // help  
  WL_WORKLOAD_NAME_HELP : "The name of the workload.",
  WL_TOO_LNAME_HELP : "The name of the tool providing the workload (a tool might provide different workloads).",
  WL_ID_HELP : "Unique id for this workload (not modifiable).",
  WL_CATEGORIES_HELP : "Used to support search and filtering of workloads.",
  WL_DESCRIPTION_HELP : "A description for this workload.",
  WL_INSTALL_SCRIPTS_HELP : "Install scripts are executed just after the provisioning of the virtual machine, usually to download and install a benchmarking tool.",
  WL_POSTINSTALL_SCRIPTS_HELP : "Post-install scripts are executed just after install scripts to accomodate, for example, platform-specific configuration.",
  WL_EXECUTE_SCRIPTS_HELP : "Execute scripts are executed to perform the benchmark of the environment.",
  WL_CLEANUP_SCRIPTS_HELP : "  Cleanup scripts are executed for multi-benchmark execution to ensure a clean environment for following benchmarks.",
  WL_USER_SCRIPTS_HELP : "Other scripts that are invoked by above scripts.",
  WL_WORKLOAD_PARAMETERS_HELP : "Parameters for the above scripts are set here.",

  WL_PARAMETER_NAME_HELP : "The name of the parameter",
  WL_PARAMETER_VALUE_HELP : "The value of the parameter",  
  
  WL_SCRIPT_NAME_HELP : "The name of the script",
  WL_SCRIPT_VALUE_HELP : "The commands to execute"
  
  
  
  
  
}

