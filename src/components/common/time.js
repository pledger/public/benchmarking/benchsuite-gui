/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const formatDuration = function(seconds) {
  var out = "";
  var fill = false;

  var days = Math.floor(seconds/60/60/24);
  if(days>0) {
    out += days+"d ";
    fill = true;
  }
  seconds = seconds-days*3600*24;
  
  var hours = Math.floor(seconds/60/60);
  if(fill || hours>0) {
    if(fill && hours<10)
      hours = "0"+hours;
    out += hours+"h ";
    fill = true;
    seconds = seconds-hours*3600;
  }
  
  var mins = Math.floor(seconds/60);
  if(fill || mins>0) {
    if(fill && mins<10)
      mins = "0"+mins;
    out += mins+"' ";
    fill = true;
    seconds = seconds-mins*60;
  }
  
  var secs = Math.floor(seconds);
  if(fill || secs>0) {
    if(mins>0 && secs<10)
      secs="0"+secs;
    out += secs+"\" ";
    fill = true;
  }
  
  return out.trim();
}