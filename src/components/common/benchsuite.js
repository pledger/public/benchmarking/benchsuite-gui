/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const getProviderSharingLevel = (provider) => {
  if(!provider.grants)
    return null;
  if(provider.grants.owner!=null && provider.grants.viewer=='org:public')
    return "public";
  if(provider.grants.owner!=null && provider.grants.viewer==null)
    return "private";
  if(provider.grants.owner==null && provider.grants.viewer=='org:public')
    return "shared";
  console.log("ERROR: unknown sharing policys for " + provider)
  return null;
}

export const getWorkloadSharingLevel = (workload) => {
  if(!workload.grants)
    return null;
  else if(workload.grants.owner!=null && workload.grants.executor=='org:public')
    return "public";
  else if(workload.grants.owner!=null && workload.grants.executor==null)
    return "private";
  else if(workload.grants.owner==null && workload.grants.executor=='org:public')
    return "shared";
  else {
    console.log("ERROR: unknown sharing policy for workload")
    return null;
  }
}

export const getScheduleSharingLevel = (schedule) => {
  if(!schedule.grants)
    return null;
  else if(schedule.grants.owner!=null && schedule.grants.viewer=='org:public')
    return "public";
  else if(schedule.grants.owner!=null && (schedule.grants.viewer==null || schedule.grants.viewer!="org:public"))
    return "private";
  else if(schedule.grants.owner==null && schedule.grants.viewer=='org:public')
    return "shared";
  else {
    console.log("ERROR: unknown sharing policy for schedule")
    return null;
  }
}

export const getBenchmarkResultsSharingLevel = (schedule) => {
  if(!schedule["reports-scope"])
    return "private";
  if(schedule["reports-scope"].startsWith("usr:"))
    return "private";
  if(schedule["reports-scope"]=="org:public")
    return "public";
  if(schedule["reports-scope"].startsWith("org:"))
    return "shared";
  console.log("ERROR: unknown results sharing policy for schedule")
  return null;
}

export const extractSharedWithOrg = (schedule) => {
  if(!schedule["reports-scope"])
    return "";
  var sharedWith = schedule["reports-scope"];
  if(sharedWith.startsWith("usr:"))
    return "";
  if(sharedWith=="org:public")
    return "";
  if(sharedWith.startsWith("org:"))
    return sharedWith.substring(4);
  return "";
}

export const trimStringValues = (dict) => {
   for(var k in dict) {
     var value = dict[k];
     if(typeof value === 'string' || value instanceof String) {
      dict[k] = value.trim();
     }
   }
   return dict;
 }

