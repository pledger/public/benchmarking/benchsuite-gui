/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const buildResponse = function(log, prefix) {
  if(log!=null) {
    if(log.trim()=="") {
      return "<span style='color:gray; font-style:italic'>"+prefix+" is empty</span>"
    } else {
      return log.replace(/\n/g, '<br/>');
    }
  } else {
    return "" //"<span style='color:gray; font-style:italic'>"+prefix+" is not available</span>"
  }
}

export const logFromRunObject = function(run, stream) {
  var log = null;
  let obj = run.results ? run.results : run.errors
  let job = obj.status.steps.run

  if (job.components) {
    // get the first job
    let job_name = Object.keys(job.components)[0];
    return buildResponse(job.components[job_name][stream])
  }

  // legacy approach
  if(obj && obj.status && obj.status.executor_info && obj.status.executor_info.components){
    let num_compos = Object.keys(obj.status.executor_info.components).length
    // TODO: hacks to find the log to show... We should implement a way to show all of them (e.g. a tab viewer)
    if(num_compos == 1){
      log = obj.status.executor_info.components[Object.keys(obj.status.executor_info.components)[0]][stream]
    } else{
      if(Object.prototype.hasOwnProperty.call(obj.status.executor_info.components, 'run-0')){
        log = obj.status.executor_info.components['run-0'][stream]
      } else {
        let n = Object.keys(obj.status.executor_info.components)[0]
        log = obj.status.executor_info.components[n][stream]
      }
    }
  }
  return buildResponse(log, "log");
}
