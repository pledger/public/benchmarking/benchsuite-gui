/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Vue from 'vue'
import VueRouter from 'vue-router'

import CurrentPath from '../components/common/CurrentPath.vue';

Vue.use(VueRouter)

const routes = [

  /***** HOME *****/

  {
    path: '/',
    name: 'Home',
    component: () => import('../components/home/Welcome.vue')
  },
  {
    path: '/welcome',
    name: 'Welcome',
    component: () => import('../components/home/Welcome.vue')
  },

  /***** PROVIDERS *****/

  {
    path: '/providers',
    name: 'Providers',
    components: {
     default: () => import('../components/providers/ProvidersList.vue'),
     bc: CurrentPath
    },
    props: {
      bc:{l1:"Providers"}
    }
  },
  {
    path: '/providers/:id/view',
    name: 'ViewProvider',
    components: {
     default: () => import('../components/providers/ProviderViewer.vue'),
     bc: CurrentPath
    },
    props: {
      default: route => ({providerId: route.params.id}),
      bc: route => ({l1: "Providers", l2: route.params.id })
    }
  },
  {
    path: '/providers/:id/edit',
    name: 'EditProvider',
    components: {
      default: () => import('../components/providers/ProviderEditor.vue'),
      bc: CurrentPath
    },
    props: {
      default: route => ({ providerId: route.params.id }),
      bc: route => ({l1: "Providers", l2: route.params.id })
    }
  },
  {
    path: '/providers/new',
    name: 'NewProvider',
    components: {
      default: () => import('../components/providers/ProviderEditor.vue'),
      bc: CurrentPath
    },
    props: {
      default: function(route) {
        if(route.query.cloneFrom) {
          return {
            templateProviderId: route.query.cloneFrom
          }
        } else {
          return {
            provider: {name:'New Provider', description:'Default description', grants:{owner:'me'}, driver:'openstack'}
          }
        }
      },
      bc: route => ({l1: "Providers", l2: route.params.id || "New Provider" })
    }
  },
  {
    path: '/providers/:id',
    name: 'ViewProvider',
    components: {
     default: () => import('../components/providers/ProviderViewer.vue'),
     bc: CurrentPath
    },
    props: {
      default: route => ({providerId: route.params.id}),
      bc:route => ({l1: "Providers", l2: route.params.id})
    }
  },
  
  /***** WORKLOADS *****/
  {
    path: '/workloads',
    name: 'Workloads',
    components: {
     default: () => import('../components/workloads/WorkloadsList.vue'),
     bc: CurrentPath
    },
    props: {
      bc:{l1:"Workloads"}
    }
  },
  {
    path: '/workloads/:id/edit',
    name: 'EditWorkload',
    components: {
      default: () => import('../components/workloads/WorkloadEditor.vue'),
      bc: CurrentPath
    },
    props: {
      default: route => ({ workloadId: route.params.id }),
      bc: route => ({l1: "Workloads", l2: route.params.id })
    }
  },
  {
    path: '/workloads/new',
    name: 'NewWorkload',
    components: {
      default: () => import('../components/workloads/WorkloadEditor.vue'),
      bc: CurrentPath
    },
    props: {
      default: function(route) {
        if(route.query.cloneFrom) {
          return {
            templateWorkloadId: route.query.cloneFrom
          }
        } else if(route.query.specialize) {
          return {
            workloadTemplate: {parent_workload_id:route.query.specialize, categories: [], grants:{owner:'me'}, workload_name:'FIXME: New Workload', tool_name:'FIXME tool name'}
          }
        } else {
          return {
            workloadTemplate: {categories: [], grants:{owner:'me'}, workload_name:'FIXME: New Workload', tool_name:'FIXME tool name'}
          }
        }
      },
      bc: route => ({l1: "Workloads", l2: route.params.id || "New Workload" })
    }
  },
  {
    path: '/workloads/:id',
    name: 'ViewWorkload',
    components: {
     default: () => import('../components/workloads/WorkloadViewer.vue'),
     bc: CurrentPath
    },
    props: {
      default: route => ({workloadId: route.params.id}),
      bc:route => ({l1: "Workloads", l2: route.params.id})
    }
  },

  /***** EXECUTIONS *****/

  {
    path: '/executions',
    name: 'Executions',
    components: {
     default: () => import('../components/executions/ExecutionsList.vue'),
     bc: CurrentPath
    },
    props: {
      bc:{l1:"Executions"}
    }
  },
  {
    path: '/executions/new',
    name: 'NewExecution',
    components: {
      default: () => import('../components/schedules/ScheduleEditor.vue'),
      bc: CurrentPath
    },
    props: {
      default: function(route) {
        if(route.query.cloneFrom) {
          return {
            executionId: route.query.cloneFrom,
            onetime:true
          }
        } else {
          return {
            scheduleTemplate:{name:'_hidden_schedule', id:null, grants:{owner:'me'}, tags:['hidden'], active:true, scheduling_hints: {onetime: true}},
            onetime:true
          };
        }
      },
      bc: route => ({l1: "Executions", l2: route.params.id || "New Execution" })
    }
  },
  {
    path: '/executions/:id',
    name: 'ViewExecution',
    components: {
     default: () => import('../components/executions/ExecutionViewer.vue'),
     bc: CurrentPath
    },
    props: {
      default: route => ({executionId: route.params.id}),
      bc:route => ({l1: "Executions", l2: route.params.id})
    }
  },

  /***** SCHEDULES *****/
  {
    path: '/schedules',
    name: 'Schedules',
    components: {
     default: () => import('../components/schedules/SchedulesList.vue'),
     bc: CurrentPath
    },
    props: {
      bc:{l1:"Schedules"}
    }
  },
  {
    path: '/schedules/:id/edit',
    name: 'EditSchedule',
    components: {
      default: () => import('../components/schedules/ScheduleEditor.vue'),
      bc: CurrentPath
    },
    props: {
      default: route => ({ scheduleId: route.params.id }),
      bc: route => ({l1: "Schedules", l2: route.params.id })
    }
  },
  {
    path: '/schedules/new',
    name: 'NewSchedule',
    components: {
      default: () => import('../components/schedules/ScheduleEditor.vue'),
      bc: CurrentPath
    },
    props: {
      default: function(route) {
        if(route.query.cloneFrom) {
          return {
            templateScheduleId: route.query.cloneFrom
          }
        } else {
          return {
            scheduleTemplate: {categories: [], grants:{owner:'me'}, name:'FIXME: New Schedule', scheduling_hints: {interval: {}}}
          }
        }
      },
      bc: route => ({l1: "Schedules", l2: route.params.id || "New Schedule" })
    }
  },
  {
    path: '/schedules/:id',
    name: 'ViewSchedule',
    components: {
     default: () => import('../components/schedules/ScheduleViewer.vue'),
     bc: CurrentPath
    },
    props: {
      default: route => ({scheduleId: route.params.id}),
      bc:route => ({l1: "Schedules", l2: route.params.id})
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
