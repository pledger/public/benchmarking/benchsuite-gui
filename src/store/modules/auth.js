/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  AUTH_REQUEST,
  AUTH_ERROR,
  AUTH_SUCCESS,
} from "../actions/auth";
import { USER_REQUEST } from "../actions/user";
import getEnv from '@/utils/env'

const state = {
  status: "",
  hasLoadedOnce: false
};

const getters = {
  isAuthenticated: state => state.status=="success",
  authStatus: state => state.status
};

const actions = {
  [AUTH_REQUEST]: ({ commit, dispatch } ) => {
      commit(AUTH_REQUEST);
      window.axios
        // this.$REST_BASE_URL is not available, because this does not exist here
        .get(getEnv('VUE_APP_BENCHSUITE_API_BASE_URL') + "/private/authenticated")
        .then(response => {
          if(response.data.authenticated) {
            commit(AUTH_SUCCESS, response);
            dispatch(USER_REQUEST);
          } else {
            commit(AUTH_ERROR);
          }
        })
        .catch(err => {
          console.log("authn error");
          console.log(err)
          commit(AUTH_ERROR, err);
        })
        .finally(() => {
        });
  }
};

const mutations = {
  [AUTH_REQUEST]: state => {
    state.status = "loading";
  },
  [AUTH_SUCCESS]: (state) => {
    state.status = "success";
  },
  [AUTH_ERROR]: state => {
    state.status = "error";
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
