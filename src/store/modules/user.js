/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from "../actions/user";
import Vue from "vue";
import getEnv from '@/utils/env'

const state = { status: "", profile: {} };

const getters = {
  getProfile: state => state.profile,
  isProfileLoaded: state => !!state.profile.name
};

const actions = {
  [USER_REQUEST]: ({ commit,  }) => {
    commit(USER_REQUEST);

    window.axios
      // this.$REST_BASE_URL is not available, because this does not exist here
      .get(getEnv('VUE_APP_BENCHSUITE_API_BASE_URL') + '/private/profile')
      .then(response => {
          commit(USER_SUCCESS, response.data);
      })
      .catch(() => {
        commit(USER_ERROR);
      })
      .finally(() => {
      });

  }

};

const mutations = {
  [USER_REQUEST]: state => {
    state.status = "loading";
  },
  [USER_SUCCESS]: (state, resp) => {
    state.status = "success";
    Vue.set(state, "profile", resp);
  },
  [USER_ERROR]: state => {
    state.status = "error";
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
