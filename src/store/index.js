/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/user";
import auth from "./modules/auth";
Vue.use(Vuex);

// TODO: reimplement this with https://medium.com/@LoCascioNick/create-a-global-loading-progress-indicator-using-vuex-axios-and-nprogress-20451b33145a
export default new Vuex.Store({

  state: {
    loading: false
  },
  modules: {
    user,
    auth
  },
  mutations: {
    fbkon (state) {
      state.loading=true;
    },
    fbkoff (state) {
      state.loading=false;
    }
  },
  strict: true  
})

