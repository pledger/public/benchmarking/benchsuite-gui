/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import getEnv from '@/utils/env'

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
  themes: {
    light: {
      primary: getEnv('VUE_APP_BENCHSUITE_GUI_THEME_PRIMARY','#1976D2'),
      secondary: getEnv('VUE_APP_BENCHSUITE_GUI_THEME_SECONDARY', '#424242'),
      accent: getEnv('VUE_APP_BENCHSUITE_GUI_THEME_ACCENT', '#82B1FF')
   },
  },
}
});
