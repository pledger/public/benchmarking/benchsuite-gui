# Benchsuite GUI
User and admin portal for the Benchmarking Suite

# Documentation
User and technical documentation is available at http://benchmarking-suite.readthedocs.io/.

# Support
For bugs, enhancements or support go to https://gitlab.res.eng.it/benchsuite

# Legal
The Benchmarking Suite is released under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.

Copyright © 2014-2022 Engineering Ingegneria Informatica S.p.A. All rights reserved.

| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received partial funding by the European Commission under grant agreements No. FP7-317859, No. H2020-732258 and No. H2020-871536 and by  EIT Digital within the EasyCloud innovation activity. |
|---|---|