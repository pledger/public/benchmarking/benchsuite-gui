#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

FROM nginx:1.19.8

COPY dist /usr/share/nginx/html

ADD nginx.conf.template /etc/nginx/templates/default.conf.template


# adds continuous integration commit reference
ARG ci_build_ref
RUN echo "$ci_build_ref" > /ci_build_ref
ENV BUILD_REF $ci_build_ref

# to make compatible with old deployment
ENV BENCHSUITE_API_BASE_URL /api/v3

COPY entrypoint.sh /usr/share/nginx/

EXPOSE 80

ENTRYPOINT ["/usr/share/nginx/entrypoint.sh"]
CMD ["/docker-entrypoint.sh", "nginx", "-g", "daemon off;"]
