#!/usr/bin/env bash
#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

# inject dynamic configuration in index.html
# source: https://medium.com/js-dojo/vue-js-runtime-environment-variables-807fa8f68665
JSON_STRING='window.configs = { \
  "VUE_APP_BENCHSUITE_API_BASE_URL":"'"${BENCHSUITE_API_BASE_URL}"'", \
  "VUE_APP_BENCHSUITE_GUI_BRAND_TITLE":"'"${BENCHSUITE_GUI_BRAND_TITLE}"'", \
  "VUE_APP_BENCHSUITE_GUI_BRAND_LOGO":"'"${BENCHSUITE_GUI_BRAND_LOGO}"'", \
  "VUE_APP_BENCHSUITE_GUI_THEME_PRIMARY":"'"${BENCHSUITE_GUI_THEME_PRIMARY}"'", \
  "VUE_APP_BENCHSUITE_GUI_THEME_SECONDARY":"'"${BENCHSUITE_GUI_THEME_SECONDARY}"'", \
  "VUE_APP_BENCHSUITE_GUI_THEME_ACCENT":"'"${BENCHSUITE_GUI_THEME_ACCENT}"'", \
  "VUE_APP_BUILD_REF":"'"${BUILD_REF}"'" \
}'

sed -i "s@// CONFIGURATIONS_PLACEHOLDER@${JSON_STRING}@" /usr/share/nginx/html/index.html

echo "=== Configuration ==="
echo $JSON_STRING
echo "=== Running ==="
exec "$@"
